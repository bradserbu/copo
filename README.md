# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Copo provides a set of a services/libraries for hosting Nodus based applications.
- Configuration
- Events
- Logging
- Performance
- Tracing

It can:
- Run applications using docker containers.
- Run applications on a cloud provider. (Azure, AWS, Google Cloud Platform, etc.)

Capabilities:
- Start/Stop Services
- Run application commands
- Manage/Monitor application resources

Provides:
- Failover and Disaster Recovery
- Scalability Deploying Multiple Instances

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact